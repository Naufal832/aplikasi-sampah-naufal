package com.example.sampah;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void mWarga(View view) {
        Intent pindah = new Intent(MainActivity.this,Login_warga.class);
        startActivity(pindah);
    }


    public void mPetugas(View view) {
        Intent pindah = new  Intent( MainActivity.this,Login_petugas.class);
        startActivity(pindah);
    }
}
